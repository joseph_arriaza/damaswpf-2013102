﻿Public Class Reglas
    Dim claseInicias As MainWindow
#Region "Click a la ficha y recibir clase"
    'RECIBE LA CLASE ANTERIOR PARA USAR SUS FUNCIONES
    'SE UTILIZA PARA OBTENER METODOS DE LA CLASE MAINWINDOW
    Public Sub RecibirClase(ByRef clase As MainWindow)
        Me.claseInicias = clase
    End Sub
    'METODO QUE PASA CUANDO SE LE DA CLICK A UNA FICHA PARA DAR SUS POSICIONES PERMITIDAS
    'SE UTILIZA CUANDO SE DA CLICK A UNA FICHA Y MARCAR DE COLOR AZUL AS POSICIONES PERMITIDAS Y LLAMAR A LOS SALTOS
    Public Sub GreenEnter(sender As Object, e As MouseButtonEventArgs)
        claseInicias.LoadColors()
        claseInicias.verificacionEspacio = False
        Dim poscionEspacio As Integer = 0
        For cont2 = 1 To 121
            If (Canvas.GetLeft(claseInicias.elipArray(cont2)) = Canvas.GetLeft(sender)) And (Canvas.GetTop(claseInicias.elipArray(cont2)) = Canvas.GetTop(sender)) Then
                claseInicias.espacioA = cont2
            End If
        Next
        Try
            claseInicias.objecto = sender
        Catch ex As Exception

        End Try
        For cont = 1 To 121
            If claseInicias.espacioOcupado(cont) = False Then
                If (Canvas.GetLeft(sender) + 9 = Canvas.GetLeft(claseInicias.elipArray(cont)) Or Canvas.GetLeft(sender) - 9 = Canvas.GetLeft(claseInicias.elipArray(cont)) Or Canvas.GetLeft(sender) + 11 = Canvas.GetLeft(claseInicias.elipArray(cont)) Or Canvas.GetLeft(sender) - 11 = Canvas.GetLeft(claseInicias.elipArray(cont)) _
                    Or Canvas.GetLeft(sender) + 7 = Canvas.GetLeft(claseInicias.elipArray(cont)) Or Canvas.GetLeft(sender) + 8 = Canvas.GetLeft(claseInicias.elipArray(cont)) Or Canvas.GetLeft(sender) - 10 = Canvas.GetLeft(claseInicias.elipArray(cont)) _
                    Or Canvas.GetLeft(sender) - 8 = Canvas.GetLeft(claseInicias.elipArray(cont)) Or Canvas.GetLeft(sender) + 10 = Canvas.GetLeft(claseInicias.elipArray(cont)) _
                    Or Canvas.GetLeft(sender) - 7 = Canvas.GetLeft(claseInicias.elipArray(cont))) _
                    And (Canvas.GetTop(sender) + 18 = Canvas.GetTop(claseInicias.elipArray(cont)) Or Canvas.GetTop(sender) - 18 = Canvas.GetTop(claseInicias.elipArray(cont))) _
                    Or (Canvas.GetTop(sender) = Canvas.GetTop(claseInicias.elipArray(cont)) And (Canvas.GetLeft(claseInicias.elipArray(cont)) = Canvas.GetLeft(sender) + 18 Or Canvas.GetLeft(claseInicias.elipArray(cont)) = Canvas.GetLeft(sender) - 18)) _
                    Then
                    claseInicias.verificacionEspacio = True
                    claseInicias.elipArray(cont).Fill = Brushes.Blue
                    claseInicias.espacios(poscionEspacio) = cont
                    claseInicias.espacioN = cont
                    AddHandler claseInicias.elipArray(cont).MouseLeftButtonDown, AddressOf ClickMove
                    poscionEspacio += 1
                End If
            End If
        Next
        SaltarArribaIzq(sender, claseInicias.espacioA, 0)
        SaltarArribaDrcha(sender, claseInicias.espacioA, 0)
        SaltarAbajoIzq(sender, claseInicias.espacioA, poscionEspacio)
        SaltarAbajoDrCha(sender, claseInicias.espacioA, poscionEspacio)
        SaltarDerecha(sender, claseInicias.espacioA, poscionEspacio)
        SaltarIzquierda(sender, claseInicias.espacioA, poscionEspacio)
        For cont = 1 To 121
            If claseInicias.elipArray(cont).Fill.Equals(Brushes.Blue) Then
            Else
                AddHandler claseInicias.elipArray(cont).MouseLeftButtonDown, AddressOf ClickErr
            End If
        Next
    End Sub
    'MENSAJE DE POSICION NO VALIDA
    Public Sub ClickErr(sender As Object, e As EventArgs)
        MsgBox("Poscion no valida ", MsgBoxStyle.Exclamation)
    End Sub
#End Region

#Region "Saltos hacia arriba"
    'METODO QUE PERMITE SALTAR HACIA ARRIBA AL LADO IZQUIERDO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub SaltarArribaIzq(sender As Object, miPos As Integer, poscionEspacio As Integer)
        If miPos = 3 Or miPos = 4 Then
            EjecutarSaltoArriba(sender, miPos, 3, 5, 0)
        ElseIf miPos = 7 Then
            EjecutarSaltoArriba(sender, miPos, 2, 3, 0)
        ElseIf miPos > 16 And miPos < 20 Then
            EjecutarSaltoArriba(sender, miPos, -15, -12, 0)
        ElseIf miPos > 28 And miPos < 33 Then
            EjecutarSaltoArriba(sender, miPos, -13, -28, 0)
        ElseIf miPos > 35 And miPos < 47 Then
            EjecutarSaltoArriba(sender, miPos, -12, -25, 0)
        ElseIf miPos > 47 And miPos < 57 Then
            EjecutarSaltoArriba(sender, miPos, -11, -23, 0)
        ElseIf miPos > 56 And miPos < 66 Then
            EjecutarSaltoArriba(sender, miPos, -10, -21, 0)
        ElseIf miPos > 66 And miPos < 76 Then
            EjecutarSaltoArriba(sender, miPos, -10, -20, 0)
        ElseIf miPos > 77 And miPos < 87 Then
            EjecutarSaltoArriba(sender, miPos, -11, -21, 0)
        ElseIf miPos > 86 And miPos < 97 Then
            EjecutarSaltoArriba(sender, miPos, -11, -21, 0)
        ElseIf miPos > 100 And miPos < 112 Then
            EjecutarSaltoArriba(sender, miPos, -13, -25, 0)
        ElseIf miPos > 111 And miPos < 116 Then
            EjecutarSaltoArriba(sender, miPos, -9, -22, 0)
        ElseIf miPos > 115 And miPos < 119 Then
            EjecutarSaltoArriba(sender, miPos, -4, -13, 0)
        ElseIf miPos = 119 Or miPos = 120 Then
            EjecutarSaltoArriba(sender, miPos, -3, -7, 0)
        ElseIf miPos = 121 Then
            EjecutarSaltoArriba(sender, miPos, -2, -5, 0)
        End If
    End Sub
    'METODO QUE PERMITE SALTAR HACIA ARRIBA AL LADO DERECHO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub SaltarArribaDrcha(sender As Object, miPos As Integer, poscionEspacio As Integer)
        If miPos = 1 Or miPos = 2 Then
            EjecutarSaltoArriba(sender, miPos, 4, 7, 0)
        ElseIf miPos > 4 And miPos < 8 Then
            EjecutarSaltoArriba(sender, miPos, 3, 5, 0)
        ElseIf miPos = 8 Then
            EjecutarSaltoArriba(sender, miPos, 3, 2, 0)
        ElseIf miPos > 14 And miPos < 18 Then
            EjecutarSaltoArriba(sender, miPos, -14, -10, 0)
        ElseIf miPos > 26 And miPos < 31 Then
            EjecutarSaltoArriba(sender, miPos, -12, -26, 0)
        ElseIf miPos > 35 And miPos < 47 Then
            EjecutarSaltoArriba(sender, miPos, -11, -23, 0)
        ElseIf miPos > 46 And miPos < 57 Then
            EjecutarSaltoArriba(sender, miPos, -10, -21, 0)
        ElseIf miPos > 56 And miPos < 66 Then
            EjecutarSaltoArriba(sender, miPos, -9, -19, 0)
        ElseIf miPos > 65 And miPos < 75 Then
            EjecutarSaltoArriba(sender, miPos, -9, -18, 0)
        ElseIf miPos > 75 And miPos < 85 Then
            EjecutarSaltoArriba(sender, miPos, -10, -19, 0)
        ElseIf miPos > 88 And miPos < 99 Then
            EjecutarSaltoArriba(sender, miPos, -12, -23, 0)
        ElseIf miPos > 98 And miPos < 110 Then
            EjecutarSaltoArriba(sender, miPos, -12, -23, 0)
        ElseIf miPos > 111 And miPos < 116 Then
            EjecutarSaltoArriba(sender, miPos, -8, -20, 0)
        ElseIf miPos > 115 And miPos < 119 Then
            EjecutarSaltoArriba(sender, miPos, -3, -11, 0)
        ElseIf miPos = 119 Or miPos = 120 Then
            EjecutarSaltoArriba(sender, miPos, -2, -5, 0)
        ElseIf miPos = 121 Then
            EjecutarSaltoArriba(sender, miPos, -1, -3, 0)
        End If
    End Sub
    'METODO QUE EJECUTA LA FUNCION SALTAR AL LADO DE ARRIBA
    'EJECUTA EL SALTO SEGUN LA POSICION QUE SE ENVIA, Y DEVUELVE UNA NUEVA POSICION
    Public Sub EjecutarSaltoArriba(sender As Object, miPos As Integer, numero As Integer, numero2 As Integer, numeroOp As Integer)
        On Error GoTo Err
        If claseInicias.espacioOcupado(miPos + numero) And claseInicias.espacioOcupado(miPos + numero2) = False Then
            claseInicias.elipArray(miPos + numero2).Fill = Brushes.Blue
            AddHandler claseInicias.elipArray(miPos + numero2).MouseLeftButtonDown, AddressOf ClickMove
            SaltarArribaIzq(sender, miPos + numero2, 0)
            SaltarArribaDrcha(sender, miPos + numero2, 0)
            SaltarDerecha(sender, miPos + numero2, 0)
            SaltarIzquierda(sender, miPos + numero2, 0)
        End If
        Exit Sub
Err:
        'MsgBox(Err.Number)
    End Sub
#End Region

#Region "Saltos hacia abajo"
    'METODO QUE PERMITE SALTAR HACIA ABAJO AL LADO DERECHO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub SaltarAbajoDrCha(sender As Object, miPos As Integer, poscionEspacio As Integer)
        If miPos > 0 And miPos < 5 Then
            EjecutarSaltoAbajo(sender, miPos, 15, 28, 1)
        ElseIf miPos > 4 And miPos < 8 Then
            EjecutarSaltoAbajo(sender, miPos, -3, 12, 1)
        ElseIf miPos = 10 Then
            EjecutarSaltoAbajo(sender, miPos, -1, -3, 0)
        ElseIf miPos = 9 Or miPos = 8 Then
            EjecutarSaltoAbajo(sender, miPos, -2, -5, 1)
        ElseIf (miPos > 10 And miPos < 24) Then
            EjecutarSaltoAbajo(sender, miPos, 13, 25, 1)
        ElseIf miPos > 23 And miPos < 34 Then
            EjecutarSaltoAbajo(sender, miPos, 12, 23, 1)
        ElseIf miPos > 35 And miPos < 45 Then
            EjecutarSaltoAbajo(sender, miPos, 11, 21, 1)
        ElseIf miPos > 46 And miPos < 56 Then
            EjecutarSaltoAbajo(sender, miPos, 10, 20, 1)
        ElseIf miPos > 56 And miPos < 66 Then
            EjecutarSaltoAbajo(sender, miPos, 10, 21, 1)
        ElseIf miPos > 65 And miPos < 75 Then
            EjecutarSaltoAbajo(sender, miPos, 11, 23, 1)
        ElseIf miPos > 75 And miPos < 87 Then
            EjecutarSaltoAbajo(sender, miPos, 12, 25, 1)
        ElseIf miPos > 89 And miPos < 94 Then
            EjecutarSaltoAbajo(sender, miPos, 13, 22, 1)
        ElseIf miPos > 113 And miPos < 116 Then
            EjecutarSaltoAbajo(sender, miPos, 3, 5, 1)
        ElseIf miPos = 116 Then
            EjecutarSaltoAbajo(sender, miPos, 3, 5, 1)
        End If
    End Sub
    'METODO QUE PERMITE SALTAR ABAJO HACIA ABAJO AL LADO IZQUIERDO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub SaltarAbajoIzq(sender As Object, miPos As Integer, poscionEspacio As Integer)
        If miPos > 0 And miPos < 5 Then
            EjecutarSaltoAbajo(sender, miPos, 14, 26, 0)
        ElseIf miPos > 4 And miPos < 8 Then
            EjecutarSaltoAbajo(sender, miPos, -4, 10, 0)
        ElseIf miPos = 10 Then
            EjecutarSaltoAbajo(sender, miPos, -2, -5, 0)
        ElseIf miPos = 8 Or miPos = 9 Then
            EjecutarSaltoAbajo(sender, miPos, -3, -7, 0)
        ElseIf (miPos > 10 And miPos < 24) Then
            EjecutarSaltoAbajo(sender, miPos, 12, 23, 0)
        ElseIf miPos > 25 And miPos < 36 Then
            EjecutarSaltoAbajo(sender, miPos, 11, 21, 0)
        ElseIf miPos > 37 And miPos < 47 Then
            EjecutarSaltoAbajo(sender, miPos, 10, 19, 0)
        ElseIf miPos > 47 And miPos < 57 Then
            EjecutarSaltoAbajo(sender, miPos, 9, 18, 0)
        ElseIf miPos > 56 And miPos < 66 Then
            EjecutarSaltoAbajo(sender, miPos, 9, 20, 0)
        ElseIf miPos > 65 And miPos < 75 Then
            EjecutarSaltoAbajo(sender, miPos, 10, 21, 0)
        ElseIf miPos > 75 And miPos < 87 Then
            EjecutarSaltoAbajo(sender, miPos, 11, 23, 0)
        ElseIf miPos > 91 And miPos < 96 Then
            EjecutarSaltoAbajo(sender, miPos, 12, 20, 0)
        ElseIf miPos > 102 And miPos < 106 Then
            EjecutarSaltoAbajo(sender, miPos, 9, 13, 0)
        ElseIf miPos > 111 And miPos < 114 Then
            EjecutarSaltoAbajo(sender, miPos, 4, 7, 0)
        ElseIf miPos = 118 Then
            EjecutarSaltoAbajo(sender, miPos, 2, 3, 0)
        End If
    End Sub
    'METODO QUE EJECUTA LA FUNCION SALTAR AL LADO DE ABAJO
    'MUEVE EL OBJECTO A LO POSICION ENVIADA Y DEVUELVE OTRAS PARA SER LLAMADO DE NUEVO
    Public Sub EjecutarSaltoAbajo(sender As Object, miPos As Integer, numero As Integer, numero2 As Integer, numeroOp As Integer)
        On Error GoTo Err
        If claseInicias.espacioOcupado(miPos + numero) And claseInicias.espacioOcupado(miPos + numero2) = False Then
            claseInicias.elipArray(miPos + numero2).Fill = Brushes.Blue
            AddHandler claseInicias.elipArray(miPos + numero2).MouseLeftButtonDown, AddressOf ClickMove
            SaltarAbajoIzq(sender, miPos + numero2, 0)
            SaltarAbajoDrCha(sender, miPos + numero2, 0)
            SaltarDerecha(sender, miPos + numero2, 0)
            SaltarIzquierda(sender, miPos + numero2, 0)
        End If
        Exit Sub
Err:
        'MsgBox(Err.Number)
    End Sub
#End Region

#Region "Saltar al lado derecho"
    'METODO QUE PERMITE LA SALTAR AL LADO DERECHO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub SaltarDerecha(sender As Object, miPos As Integer, poscionEspacio As Integer)
        Try
            If (miPos > 10 And miPos + 2 < 24) Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 0 And miPos + 2 < 5 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos = 5 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 23 And miPos + 2 < 36 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 35 And miPos + 2 < 47 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 46 And miPos + 2 < 57 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 56 And miPos + 2 < 66 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 65 And miPos + 2 < 75 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 74 And miPos + 2 < 98 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 97 And miPos + 2 <= 111 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos > 111 And miPos + 2 < 116 Then
                'EjecutarSaltoDer(miPos, poscionEspacio, sender)
            ElseIf miPos = 116 Then
                EjecutarSaltoDer(miPos, poscionEspacio, sender)
            End If
        Catch ex As Exception

        End Try
    End Sub
    'METODO QUE EJECUTA LA FUCION DE SALTAR AL LADO DERECHO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub EjecutarSaltoDer(miPos As Integer, poscionEspacio As Integer, sender As Object)
        If claseInicias.espacioOcupado(miPos + 1) And claseInicias.espacioOcupado(miPos + 2) = False Then
            claseInicias.elipArray(miPos + 2).Fill = Brushes.Blue
            AddHandler claseInicias.elipArray(miPos + 2).MouseLeftButtonDown, AddressOf ClickMove
            SaltarArribaIzq(sender, miPos + 2, 0)
            SaltarArribaDrcha(sender, miPos + 2, 0)
            SaltarAbajoIzq(sender, miPos + 2, 0)
            SaltarAbajoDrCha(sender, miPos + 2, 0)
            SaltarDerecha(sender, miPos + 2, poscionEspacio)
        End If
    End Sub
#End Region

#Region "Saltar al lado izquierdo"
    'METODO QUE EJECUTA LA FUNCION DE SALTAR AL LADO IZQUIERDO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub EjecutarSaltoIzq(miPos As Integer, poscionEspacio As Integer, sender As Object)
        If claseInicias.espacioOcupado(miPos - 1) And claseInicias.espacioOcupado(miPos - 2) = False Then
            claseInicias.elipArray(miPos - 2).Fill = Brushes.Blue
            AddHandler claseInicias.elipArray(miPos - 2).MouseLeftButtonDown, AddressOf ClickMove
            SaltarArribaIzq(sender, miPos - 2, 0)
            SaltarArribaDrcha(sender, miPos - 2, 0)
            SaltarAbajoIzq(sender, miPos - 2, 0)
            SaltarAbajoDrCha(sender, miPos - 2, 0)
            SaltarIzquierda(sender, miPos - 2, poscionEspacio)
        End If
    End Sub
    'METODO QUE PERMITE SALTAR A EL LADO IZQUIERDO
    'UTILIZA UNA SERIE DE IF PARA DETERMINAR SU POSICION
    Public Sub SaltarIzquierda(sender As Object, miPos As Integer, poscionEspacio As Integer)
        If (miPos - 2 > 10 And miPos < 24) Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf (miPos - 2 > 2) And miPos < 5 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos = 7 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 23 And miPos < 36 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 35 And miPos < 47 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 46 And miPos < 57 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 56 And miPos < 66 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 65 And miPos < 75 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 75 And miPos < 98 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 86 And miPos < 99 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 97 And miPos < 111 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos - 2 > 112 And miPos < 116 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        ElseIf miPos = 118 Then
            EjecutarSaltoIzq(miPos, poscionEspacio, sender)
        End If
    End Sub
#End Region

#Region "Metodos al dar click"
    'METODO QUE MUEVE LA FICHA A LA POSICION ESPECIFICA
    'INGRESA LA FICHA EN UNA POSICION Y LLAMA A VERIFICAR EL GANADOR
    Public Sub ClickMove(sender As Object, e As MouseButtonEventArgs)
        claseInicias.LoadColors()
        For cont2 = 1 To 121
            If (Canvas.GetLeft(claseInicias.elipArray(cont2)) = Canvas.GetLeft(sender)) And (Canvas.GetTop(claseInicias.elipArray(cont2)) = Canvas.GetTop(sender)) Then
                claseInicias.espacioN = cont2
            End If
        Next
        If Not (IsNothing(claseInicias.objecto)) Then
            Canvas.SetLeft(claseInicias.objecto, Canvas.GetLeft(sender))
            Canvas.SetTop(claseInicias.objecto, Canvas.GetTop(sender))
            claseInicias.Tablero.Children.Remove(claseInicias.objecto)
            claseInicias.Tablero.Children.Add(claseInicias.objecto)
            claseInicias.objecto = Nothing
        End If
        If claseInicias.cantidadJugadores = 3 Then
            claseInicias.VerGanadorRojo()
            claseInicias.VerGanadorAzul()
            claseInicias.VerGanadorNaranja()
        Else
            claseInicias.VerGanadorRojo()
            claseInicias.VerGanadorAzul()
            claseInicias.VerGanadorNaranja()
            claseInicias.VerGanadorVerde()
            claseInicias.VerGanadorAmarillo()
            claseInicias.VerGanadorNegro()
        End If
        claseInicias.espacioOcupado(claseInicias.espacioN) = True
        claseInicias.espacioOcupado(claseInicias.espacioA) = False
        If claseInicias.cantidadJugadores = 6 Then
            claseInicias.PlayGame()
        Else
            claseInicias.PlayGame3()
        End If
    End Sub
#End Region

End Class
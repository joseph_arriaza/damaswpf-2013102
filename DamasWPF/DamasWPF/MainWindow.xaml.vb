﻿Class MainWindow
#Region "Variables"
    Public claseDeReglas As New Reglas
    Public jugadorres(6) As String
    Public elipArray(121) As Ellipse
    Public greenPiecesMoving As GreenPieces
    Public redPiecesMoving As RedPieces
    Public bluePiecesMoving As BluePieces
    Public yellowPieceMoving As YellowPieces
    Public grayPieceMoving As GrayPieces
    Public orangePieceMoving As OrangePieces
    Public espacioOcupado(121) As Boolean
    Public espacios(25) As Integer
    Public espacioA, espacioN As Integer
    Public fichaMov As Integer
    Public turno As Integer = 1
    Public verificacionEspacio As Boolean
    '1 verde, 2 rojo, 3 azul
    Public cantidadJugadores As Integer = 0
    'ARRAY PIECES
    Public greenPieces(15) As GreenPieces
    Public greenPieceCount As Integer = 1
    Public redPieces(15) As RedPieces
    Public redPieceCount As Integer = 1
    Public bluePieces(15) As BluePieces
    Public bluePiecesCount As Integer = 1
    Public yellowPieces(15) As YellowPieces
    Public yellowPiecesCount As Integer = 1
    Public grayPieces(15) As GrayPieces
    Public grayPiecesCount As Integer = 1
    Public orangePieces(15) As OrangePieces
    Public orangePiecesCount As Integer = 1
    'ARRAY DE GANADORES
    Public ganadorRojo(15) As Boolean
    Public ganadorRojoCount As Integer = 1
    Public ganadorAzul(15) As Boolean
    Public ganadorNaranja(15) As Boolean
    Public ganadorVerde(10) As Boolean
    Public ganadorAmarillo(10) As Boolean
    Public ganadorNegro(10) As Boolean
    Public objecto As Object
    Public puntos(6) As Integer
    Dim posiciones(15) As Integer
#End Region

#Region "Cargar Tablero"
    'METODO QUE CARGA EL TABLERO
    'SE UTILIZA DIFERENTES ESPACIOS PARA CREAR LAS DIFERENTES PARTES DEL TABLERO
    Public Sub LoadBoard()
        Dim cantidad As Integer = 4
        Dim top As Integer = 72
        Dim left As Integer = 118
        Dim nombre As Integer = 1
        For y = 1 To 4
            For x = 1 To cantidad
                Dim green As New GreenPieces
                Dim elipS As New Ellipse
                green.setTop(top)
                green.setLeft(left)
                Canvas.SetLeft(green, left)
                Canvas.SetTop(green, top)
                elipS.Fill = Brushes.Black
                elipS.Height = 17
                elipS.Width = 17
                Canvas.SetLeft(elipS, left)
                Canvas.SetTop(elipS, top)
                elipS.Stroke = Brushes.Black
                If cantidadJugadores > 0 Then
                    elipS.Stroke = Brushes.Red
                End If
                elipS.Fill = Brushes.White
                elipS.Name = "A" & nombre
                elipArray(nombre) = elipS
                Tablero.Children.Add(elipS)
                If cantidadJugadores > 0 Then
                    If cantidadJugadores = 6 Then
                        Tablero.Children.Add(green)
                        espacioOcupado(nombre) = True
                    Else
                        espacioOcupado(nombre) = False
                    End If
                    greenPieces(greenPieceCount) = green
                    greenPieceCount += 1
                End If
                'fichaMov = 1
                'AddHandler green.MouseEnter, AddressOf GreenEnter
                left += 18
                nombre += 1
            Next
            If y = 1 Then
                left = 127
            ElseIf y = 2 Then
                left = 136
            ElseIf y = 3 Then
                left = 145
            End If
            top -= 18
            cantidad -= 1
        Next
        left = 12 + 25
        top = 90
        cantidad = 13
        For y = 1 To 9
            For x = 1 To cantidad
                Dim elipS As New Ellipse
                elipS.Fill = Brushes.White
                elipS.Stroke = Brushes.Black
                elipS.Height = 17
                elipS.Width = 17
                Canvas.SetLeft(elipS, left)
                Canvas.SetTop(elipS, top)
                Tablero.Children.Add(elipS)
                elipS.Name = "A" & nombre
                If cantidadJugadores = 3 Then
                    If (x < 6 And y = 1) Or (x < 5 And y = 2) Or (x < 4 And y = 3) Or (x < 3 And y = 4) Or (x < 2 And y = 5) Then
                        Dim blue As New BluePieces
                        Canvas.SetLeft(blue, left)
                        Canvas.SetTop(blue, top)
                        Tablero.Children.Add(blue)
                        elipS.Stroke = Brushes.Black
                        bluePieces(bluePiecesCount) = blue
                        bluePiecesCount += 1
                        espacioOcupado(nombre) = True
                    ElseIf (x > 8 And y = 1) Or (x > 8 And y = 2) Or (x > 8 And y = 3) Or (x > 8 And y = 4) Or (x > 8 And y = 5) Then
                        Dim orange As New OrangePieces
                        Canvas.SetLeft(orange, left)
                        Canvas.SetTop(orange, top)
                        Tablero.Children.Add(orange)
                        elipS.Stroke = Brushes.Black
                        orangePieces(orangePiecesCount) = orange
                        orangePiecesCount += 1
                        espacioOcupado(nombre) = True
                    ElseIf (x > 4 And x < 10) And (y = 9) Then
                        Dim red As New RedPieces
                        Canvas.SetLeft(red, left)
                        Canvas.SetTop(red, top)
                        Tablero.Children.Add(red)
                        redPieces(redPieceCount) = red
                        redPieceCount += 1
                        elipS.Stroke = Brushes.Black
                        espacioOcupado(nombre) = True
                    ElseIf (x < 5 And y = 9) Or (x < 4 And y = 8) Or (x < 3 And y = 7) Or (x < 2 And y = 6) Then
                        elipS.Stroke = Brushes.Orange
                        elipS.Fill = Brushes.White
                        espacioOcupado(nombre) = False
                    ElseIf (x > 9 And y = 9) Or (x > 9 And y = 8) Or (x > 9 And y = 7) Or (x > 9 And y = 6) Then
                        elipS.Stroke = Brushes.Blue
                        elipS.Fill = Brushes.White
                        espacioOcupado(nombre) = False
                    Else
                        elipS.Stroke = Brushes.White
                        elipS.Fill = Brushes.LightGray
                        espacioOcupado(nombre) = False
                    End If
                ElseIf cantidadJugadores = 6 Then
                    If (x < 5 And y = 1) Or (x < 4 And y = 2) Or (x < 3 And y = 3) Or (x < 2 And y = 4) Then
                        Dim blue As New BluePieces
                        Canvas.SetLeft(blue, left)
                        Canvas.SetTop(blue, top)
                        elipS.Stroke = Brushes.Black
                        If cantidadJugadores > 0 Then
                            If cantidadJugadores = 6 Then
                                Tablero.Children.Add(blue)
                            End If
                            bluePieces(bluePiecesCount) = blue
                            bluePiecesCount += 1
                        End If
                        espacioOcupado(nombre) = True
                    ElseIf (x < 5 And y = 9) Or (x < 4 And y = 8) Or (x < 3 And y = 7) Or (x < 2 And y = 6) Then
                        Dim yellow As New YellowPieces
                        Canvas.SetLeft(yellow, left)
                        Canvas.SetTop(yellow, top)
                        elipS.Stroke = Brushes.Orange
                        If cantidadJugadores > 0 Then
                            If cantidadJugadores = 6 Then
                                Tablero.Children.Add(yellow)
                            End If
                            yellowPieces(yellowPiecesCount) = yellow
                            yellowPiecesCount += 1
                        End If
                        espacioOcupado(nombre) = True
                    ElseIf (x > 9 And y = 9) Or (x > 9 And y = 8) Or (x > 9 And y = 7) Or (x > 9 And y = 6) Then
                        Dim gray As New GrayPieces
                        Canvas.SetLeft(gray, left)
                        Canvas.SetTop(gray, top)
                        elipS.Stroke = Brushes.Blue
                        If cantidadJugadores > 0 Then
                            If cantidadJugadores = 6 Then
                                Tablero.Children.Add(gray)
                            End If
                            grayPieces(grayPiecesCount) = gray
                            grayPiecesCount += 1
                        End If
                        espacioOcupado(nombre) = True
                    ElseIf (x > 9 And y = 1) Or (x > 9 And y = 2) Or (x > 9 And y = 3) Or (x > 9 And y = 4) Then
                        Dim orange As New OrangePieces
                        Canvas.SetLeft(orange, left)
                        Canvas.SetTop(orange, top)
                        elipS.Stroke = Brushes.Yellow
                        If cantidadJugadores > 0 Then
                            If cantidadJugadores = 6 Then
                                Tablero.Children.Add(orange)
                            End If
                            orangePieces(orangePiecesCount) = orange
                            orangePiecesCount += 1
                        End If
                        espacioOcupado(nombre) = True
                    Else
                        elipS.Stroke = Brushes.White
                        elipS.Fill = Brushes.LightGray
                        espacioOcupado(nombre) = False
                    End If
                End If
                elipArray(nombre) = elipS
                left += 18
                nombre += 1
            Next
            If y = 1 Then
                left = 46
            ElseIf y = 2 Then
                left = 53
            ElseIf y = 3 Then
                left = 61
            ElseIf y = 4 Then
                left = 70
            ElseIf y = 5 Then
                left = 61
                cantidad = 9
            ElseIf y = 6 Then
                left = 53
            ElseIf y = 7 Then
                left = 46
            ElseIf y = 8 Then
                left = 39
            End If
            top += 18
            If y < 5 Then
                cantidad -= 1
            Else
                cantidad += 1
            End If
        Next
        left = 93 + 25
        cantidad = 4
        top = 252
        For y = 1 To 4
            For x = 1 To cantidad
                Dim red As New RedPieces
                Dim elipS As New Ellipse
                red.setTop(top)
                red.setLeft(left)
                Canvas.SetLeft(red, left)
                Canvas.SetTop(red, top)
                elipS.Fill = Brushes.Black
                elipS.Height = 17
                elipS.Width = 17
                Canvas.SetLeft(elipS, left)
                Canvas.SetTop(elipS, top)
                elipS.Stroke = Brushes.Black
                If cantidadJugadores > 0 Then
                    elipS.Stroke = Brushes.Green
                End If
                elipS.Fill = Brushes.White
                elipS.Name = "A" & nombre
                elipArray(nombre) = elipS
                espacioOcupado(nombre) = True
                Tablero.Children.Add(elipS)
                'fichaMov = 2
                If cantidadJugadores > 0 Then
                    If cantidadJugadores = 6 Then
                        Tablero.Children.Add(red)
                    ElseIf cantidadJugadores = 3 Then
                        Tablero.Children.Add(red)
                    End If
                    redPieces(redPieceCount) = red
                    redPieceCount += 1
                End If
                'AddHandler red.MouseEnter, AddressOf GreenEnter
                left += 18
                nombre += 1
            Next
            If y = 1 Then
                left = 127
            ElseIf y = 2 Then
                left = 136
            ElseIf y = 3 Then
                left = 145
            End If
            top += 18
            cantidad -= 1
        Next
    End Sub
#End Region

#Region "Metodos esenciales"
    'METODO QUE CAMBIA LOS COLORES AL COLOR PREDEFINIDO
    'VUELVE LOS COLORES AL GRIS ANTERIOR 
    Public Sub LoadColors()
        For cont = 1 To 121
            elipArray(cont).Fill = Brushes.LightGray
            RemoveHandler elipArray(cont).MouseLeftButtonDown, AddressOf claseDeReglas.ClickMove
            RemoveHandler elipArray(cont).MouseLeftButtonDown, AddressOf claseDeReglas.ClickErr
        Next
    End Sub
    'METODO QUE SE REALIZA AL INICIALIZAR EL TABLERO PARA CREAR BOTONES Y TEXTBOX PARA LOS USUARIOS
    'CREAR BOTONES DE CANTIDAD DE JUGADORES
    Public Sub Tablero_Initialized(sender As Object, e As EventArgs) Handles Board.Initialized
        claseDeReglas.RecibirClase(Me)
        btnRestart.Visibility = Windows.Visibility.Hidden
        txt1.Visibility = Windows.Visibility.Hidden
        txt2.Visibility = Windows.Visibility.Hidden
        txt3.Visibility = Windows.Visibility.Hidden
        txt4.Visibility = Windows.Visibility.Hidden
        txt5.Visibility = Windows.Visibility.Hidden
        txt6.Visibility = Windows.Visibility.Hidden
        btnAccept.Visibility = Windows.Visibility.Hidden
        LoadBoard()
    End Sub

    
#End Region

#Region "Jugar con cantidad"
    'METODO QUE SE UTILIZA CUANDO SE SELECCIONA LA OPCION DE 3 JUGADORES
    Public Sub Players3(sender As Object, e As EventArgs)
        txt1.IsEnabled = True
        txt2.IsEnabled = True
        txt3.IsEnabled = True
        txt4.IsEnabled = True
        txt5.IsEnabled = True
        txt6.IsEnabled = True
        btnRestart.Visibility = Windows.Visibility.Visible
        btn6Players.Visibility = Windows.Visibility.Hidden
        btn3Players.Visibility = Windows.Visibility.Hidden
        points.Content = ""
        txt1.ToolTip = "Ficha naranja"
        txt1.Visibility = Windows.Visibility.Visible
        txt1.Text = ""
        txt2.ToolTip = "Ficha azul"
        txt2.Visibility = Windows.Visibility.Visible
        txt2.Text = ""
        txt3.ToolTip = "Ficha roja"
        txt3.Visibility = Windows.Visibility.Visible
        txt3.Text = ""
        btnAccept.Visibility = Windows.Visibility.Visible
        cantidadJugadores = 3
    End Sub
    'METODO QUE SE UTILIZA CUANDO SE SELECCION LA OPCION DE 6 JUGADORES
    Public Sub Players6(sender As Object, e As EventArgs)
        btnRestart.Visibility = Windows.Visibility.Visible
        btn6Players.Visibility = Windows.Visibility.Hidden
        btn3Players.Visibility = Windows.Visibility.Hidden
        txt1.IsEnabled = True
        txt2.IsEnabled = True
        txt3.IsEnabled = True
        txt4.IsEnabled = True
        txt5.IsEnabled = True
        txt6.IsEnabled = True
        points.Content = ""
        txt2.ToolTip = "Ficha verde"
        txt3.ToolTip = "Ficha azul"
        txt1.ToolTip = "Ficha amarilla"
        txt1.Visibility = Windows.Visibility.Visible
        txt1.Text = ""
        'txt3.ToolTip = "Ficha azul"
        txt2.Visibility = Windows.Visibility.Visible
        txt2.Text = ""
        'txt1.ToolTip = "Ficha amarilla"
        txt3.Visibility = Windows.Visibility.Visible
        txt3.Text = ""
        ' txt4.ToolTip = "Ficha roja"
        txt4.Visibility = Windows.Visibility.Visible
        txt4.Text = ""
        ' txt5.ToolTip = "Ficha negra"
        txt5.Visibility = Windows.Visibility.Visible
        txt5.Text = ""
        'txt6.ToolTip = "Ficha naranja"
        txt6.Visibility = Windows.Visibility.Visible
        txt6.Text = ""
        btnAccept.Visibility = Windows.Visibility.Visible
        cantidadJugadores = 6
    End Sub
    'METODO QUE SE UTILIZA PARA JUGAR CON 3 PERSONAS
    Public Sub PlayGame3()
        If turno = 1 Then
            rctColor.Fill = Brushes.Blue
            fichaMov = 2
            For cont = 1 To 15
                RemoveHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                AddHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno += 1
        ElseIf turno = 2 Then
            rctColor.Fill = Brushes.Red
            fichaMov = 4
            For cont = 1 To 15
                AddHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno += 1
        ElseIf turno = 3 Then
            rctColor.Fill = Brushes.Orange
            fichaMov = 6
            For cont = 1 To 15
                RemoveHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                AddHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno = 1
        End If
    End Sub
    'METODO QUE SE UTILIZA PARA JUGAR CON 6 PERSONAS
    Public Sub PlayGame()
        If turno = 1 Then
            rctColor.Fill = Brushes.Green
            fichaMov = 1
            For cont = 1 To 10
                AddHandler greenPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler yellowPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler grayPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno += 1
        ElseIf turno = 2 Then
            rctColor.Fill = Brushes.Blue
            fichaMov = 2
            For cont = 1 To 10
                AddHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler greenPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler yellowPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler grayPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno += 1
        ElseIf turno = 3 Then
            rctColor.Fill = Brushes.Yellow
            fichaMov = 3
            For cont = 1 To 10
                RemoveHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler greenPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                AddHandler yellowPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler grayPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno += 1
        ElseIf turno = 4 Then
            rctColor.Fill = Brushes.Red
            fichaMov = 4
            For cont = 1 To 10
                RemoveHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler greenPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                AddHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler yellowPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler grayPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno += 1
        ElseIf turno = 5 Then
            rctColor.Fill = Brushes.Black
            fichaMov = 5
            For cont = 1 To 10
                RemoveHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler greenPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler yellowPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                AddHandler grayPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno += 1
        ElseIf turno = 6 Then
            rctColor.Fill = Brushes.Orange
            fichaMov = 6
            For cont = 1 To 10
                RemoveHandler bluePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler greenPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler redPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler yellowPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                RemoveHandler grayPieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
                AddHandler orangePieces(cont).MouseLeftButtonDown, AddressOf claseDeReglas.GreenEnter
            Next
            turno = 1
        End If
    End Sub
    'METODO QUE VERIFICA QUE LOS CAMPOS DE NOMBRES DE USUARIOS NO ESTEN VACIOS
    Public Sub Aceptar(sender As Object, e As EventArgs)
        turno = 1
        greenPieceCount = 1
        redPieceCount = 1
        bluePiecesCount = 1
        yellowPiecesCount = 1
        grayPiecesCount = 1
        orangePiecesCount = 1
        If (txt2.Text.Trim <> "") Then
            txt2.Background = Brushes.White
            txt2.Foreground = Brushes.Black
            If (txt3.Text.Trim <> "") Then
                txt3.Background = Brushes.White
                txt3.Foreground = Brushes.Black
                If (txt1.Text.Trim <> "") Then
                    jugadorres(1) = txt2.Text
                    jugadorres(2) = txt3.Text
                    jugadorres(3) = txt1.Text
                    txt1.Background = Brushes.White
                    txt1.Foreground = Brushes.Black
                    If cantidadJugadores = 6 Then
                        If (txt4.Text.Trim <> "") Then
                            txt4.Background = Brushes.White
                            txt4.Foreground = Brushes.Black
                            If (txt5.Text.Trim <> "") Then
                                txt5.Background = Brushes.White
                                txt5.Foreground = Brushes.Black
                                If (txt6.Text.Trim <> "") Then
                                    txt6.Background = Brushes.White
                                    txt6.Foreground = Brushes.Black
                                    jugadorres(4) = txt4.Text
                                    jugadorres(5) = txt5.Text
                                    jugadorres(6) = txt6.Text
                                    txt1.IsEnabled = False
                                    txt2.IsEnabled = False
                                    txt3.IsEnabled = False
                                    txt4.IsEnabled = False
                                    txt5.IsEnabled = False
                                    txt6.IsEnabled = False
                                    btnAccept.Visibility = Windows.Visibility.Hidden
                                    LoadBoard()
                                    PlayGame()
                                Else
                                    txt6.Background = Brushes.Red
                                    txt6.Foreground = Brushes.White
                                End If
                            Else
                                txt5.Background = Brushes.Red
                                txt5.Foreground = Brushes.White
                            End If
                        Else
                            txt4.Background = Brushes.Red
                            txt4.Foreground = Brushes.White
                        End If
                    Else
                        txt1.IsEnabled = False
                        txt2.IsEnabled = False
                        txt3.IsEnabled = False
                        btnAccept.Visibility = Windows.Visibility.Hidden
                        LoadBoard()
                        PlayGame3()
                    End If
                Else
                    txt1.Background = Brushes.Red
                    txt1.Foreground = Brushes.White
                End If
            Else
                txt3.Background = Brushes.Red
                txt3.Foreground = Brushes.White
            End If
        Else
            txt2.Background = Brushes.Red
            txt2.Foreground = Brushes.White
        End If
    End Sub

#End Region

#Region "Nueva partida"
    'CON ESTE METODO SE REINICIA LA PARTIDA AL PRESIONAR EL BOTON RESET
    Public Sub Restart(sender As Object, e As EventArgs)
        For cont = 1 To 121
            espacioOcupado(cont) = False
        Next
        btnRestart.Visibility = Windows.Visibility.Hidden
        btn6Players.Visibility = Windows.Visibility.Visible
        btn3Players.Visibility = Windows.Visibility.Visible
        txt1.Visibility = Windows.Visibility.Hidden
        txt2.Visibility = Windows.Visibility.Hidden
        txt3.Visibility = Windows.Visibility.Hidden
        txt4.Visibility = Windows.Visibility.Hidden
        txt5.Visibility = Windows.Visibility.Hidden
        txt6.Visibility = Windows.Visibility.Hidden
        btnAccept.Visibility = Windows.Visibility.Hidden
    End Sub
    'ESTE METODO SE UTILIZA CUANDO SE TERMINA UNA PARTIDA Y LE SUMA LOS PUNTOS AL GANADOR
    Public Sub NuevaPartida()
        For cont = 1 To 121
            espacioOcupado(cont) = False
        Next
        points.Foreground = Brushes.White
        If cantidadJugadores = 3 Then
            points.Content = jugadorres(1) & " = " & " " & puntos(1) & "    " & jugadorres(2) & " = " & " " & puntos(2) & "    " & jugadorres(3) & " = " & " " & puntos(3)
        Else
            points.Content = jugadorres(1) & " = " & " " & puntos(1) & "    " & jugadorres(2) & " = " & " " & puntos(2) & "    " & jugadorres(3) & " = " & " " & puntos(3) & "    " & jugadorres(4) & " = " & " " & puntos(4) & "    " & jugadorres(5) & " = " & " " & puntos(5) & "    " & jugadorres(6) & " = " & " " & puntos(6)
        End If
        greenPieceCount = 1
        bluePiecesCount = 1
        yellowPiecesCount = 1
        redPieceCount = 1
        grayPiecesCount = 1
        orangePiecesCount = 1
        turno = 1
        LoadBoard()
        If cantidadJugadores = 6 Then
            PlayGame()
        Else
            PlayGame3()
        End If
    End Sub
#End Region

#Region "Ver ganadores"
    'METODO QUE OBTIENE LAS POSCICIONES DE LAS FICHAS NEGRAS
    Public Sub VerificarPosNegra()
        Dim ganador As Integer = 1
        For cont = 1 To 10
            For cont2 = 1 To 121
                If (Canvas.GetLeft(elipArray(cont2)) = Canvas.GetLeft(grayPieces(cont))) And (Canvas.GetTop(elipArray(cont2)) = Canvas.GetTop(grayPieces(cont))) Then
                    posiciones(ganador) = cont2
                    ganador += 1
                End If
            Next
        Next
    End Sub
    Public Sub VerGanadorNegro()
        ganadorRojoCount = 1
        VerificarPosNegra()
        For cont = 1 To 10
            If posiciones(cont) = 11 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 12 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 13 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 14 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 24 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 25 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 26 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 36 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 37 Then
                ganadorNegro(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 47 Then
                ganadorNegro(ganadorRojoCount) = True
            Else
                ganadorNegro(ganadorRojoCount) = False
            End If
            ganadorRojoCount += 1
        Next
        If Ganado(10, 5) Then
            MsgBox("El jugador " & jugadorres(5) & " ha ganado la partida!!!")
            puntos(5) += 1
            NuevaPartida()
        End If
    End Sub
    'METODO QUE OBTIENE LAS POSCICIONES DE LAS FICHAS AMARILLAS
    Public Sub VerificarPosAmarilla()
        Dim ganador As Integer = 1
        For cont = 1 To 10
            For cont2 = 1 To 121
                If (Canvas.GetLeft(elipArray(cont2)) = Canvas.GetLeft(yellowPieces(cont))) And (Canvas.GetTop(elipArray(cont2)) = Canvas.GetTop(yellowPieces(cont))) Then
                    posiciones(ganador) = cont2
                    ganador += 1
                End If
            Next
        Next
    End Sub
    'METODO QUE VERIFICA SI EL GANADOR ES AMARILLO
    Public Sub VerGanadorAmarillo()
        ganadorRojoCount = 1
        VerificarPosAmarilla()
        For cont = 1 To 10
            If posiciones(cont) = 20 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 21 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 22 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 23 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 33 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 34 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 35 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 45 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 46 Then
                ganadorAmarillo(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 56 Then
                ganadorAmarillo(ganadorRojoCount) = True
            Else
                ganadorAmarillo(ganadorRojoCount) = False
            End If
            ganadorRojoCount += 1
        Next
        If Ganado(10, 3) Then
            MsgBox("El jugador " & jugadorres(3) & " ha ganado la partida!!!")
            puntos(3) += 1
            NuevaPartida()
        End If
    End Sub
    'METODO QUE OBTIENE LAS POSCICIONES DE LAS FICHAS VERDES
    Public Sub VerificarPosVerde()
        Dim ganador As Integer = 1
        For cont = 1 To 10
            For cont2 = 1 To 121
                If (Canvas.GetLeft(elipArray(cont2)) = Canvas.GetLeft(greenPieces(cont))) And (Canvas.GetTop(elipArray(cont2)) = Canvas.GetTop(greenPieces(cont))) Then
                    posiciones(ganador) = cont2
                    ganador += 1
                End If
            Next
        Next
    End Sub
    'METODO QUE VERIFICA SI EL GANADOR ES VERDE
    Public Sub VerGanadorVerde()
        ganadorRojoCount = 1
        VerificarPosVerde()
        For cont = 1 To 10
            If posiciones(cont) = 112 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 113 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 114 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 115 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 116 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 117 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 118 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 119 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 120 Then
                ganadorVerde(ganadorRojoCount) = True
            ElseIf posiciones(cont) = 121 Then
                ganadorVerde(ganadorRojoCount) = True
            Else
                ganadorVerde(ganadorRojoCount) = False
            End If
            ganadorRojoCount += 1
        Next
        If Ganado(10, 1) Then
            MsgBox("El jugador " & jugadorres(1) & " ha ganado la partida!!!")
            puntos(1) += 1
            NuevaPartida()
        End If
    End Sub
    'VERIFICA EL GANADOR
    Public Function Ganado(numero As Integer, color As Integer)
        For cont = 1 To numero
            If color = 4 Then
                If ganadorRojo(cont) = False Then
                    Return False
                End If
            ElseIf color = 2 Then
                If ganadorAzul(cont) = False Then
                    Return False
                End If
            ElseIf color = 6 Then
                If ganadorNaranja(cont) = False Then
                    Return False
                End If
            ElseIf color = 1 Then
                If ganadorVerde(cont) = False Then
                    Return False
                End If
            ElseIf color = 3 Then
                If ganadorAmarillo(cont) = False Then
                    Return False
                End If
            ElseIf color = 5 Then
                If ganadorNegro(cont) = False Then
                    Return False
                End If
            End If
        Next
        Return True
    End Function
    'METODO QUE OBTIENE LAS POSCICIONES DE LAS FICHAS NARANJAS
    Public Sub VerificarPosNaranja(numero As Integer)
        Dim ganador As Integer = 1
        For cont = 1 To numero
            For cont2 = 1 To 121
                If (Canvas.GetLeft(elipArray(cont2)) = Canvas.GetLeft(orangePieces(cont))) And (Canvas.GetTop(elipArray(cont2)) = Canvas.GetTop(orangePieces(cont))) Then
                    posiciones(ganador) = cont2
                    ganador += 1
                End If
            Next
        Next
    End Sub
    'METODO QUE VERIFICA SI EL GANADOR ES NARANJA
    Public Sub VerGanadorNaranja()
        ganadorRojoCount = 1
        If cantidadJugadores = 6 Then
            VerificarPosNaranja(10)
        Else
            VerificarPosNaranja(15)
        End If
        For cont = 1 To 15
            If cantidadJugadores = 3 Then
                If posiciones(cont) = 66 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 76 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 77 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 87 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 88 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 89 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 99 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 100 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 101 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 102 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 57 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 67 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 78 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 90 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 103 Then
                    ganadorNaranja(ganadorRojoCount) = True
                Else
                    ganadorNaranja(ganadorRojoCount) = False
                End If
            ElseIf cantidadJugadores = 6 Then
                If posiciones(cont) = 66 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 76 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 77 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 87 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 88 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 89 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 99 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 100 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 101 Then
                    ganadorNaranja(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 102 Then
                    ganadorNaranja(ganadorRojoCount) = True
                Else
                    ganadorNaranja(ganadorRojoCount) = False
                End If
            End If
            ganadorRojoCount += 1
        Next
        If cantidadJugadores = 6 Then
            If Ganado(10, 6) Then
                MsgBox("El jugador " & jugadorres(6) & " ha ganado la partida!!!")
                puntos(6) += 1
                NuevaPartida()
            End If
        Else
            If Ganado(15, 6) Then
                MsgBox("El jugador " & jugadorres(3) & " ha ganado la partida!!!")
                puntos(3) += 1
                NuevaPartida()
            End If
        End If
    End Sub
    'METODO QUE OBTIENE LAS POSCICIONES DE LAS FICHAS AZULES
    Public Sub VerificarPosAzul(numero As Integer)
        Dim ganador As Integer = 1
        For cont = 1 To numero
            For cont2 = 1 To 121
                If (Canvas.GetLeft(elipArray(cont2)) = Canvas.GetLeft(bluePieces(cont))) And (Canvas.GetTop(elipArray(cont2)) = Canvas.GetTop(bluePieces(cont))) Then
                    posiciones(ganador) = cont2
                    ganador += 1
                End If
            Next
        Next
    End Sub
    'METODO QUE VERIFICA SI EL GANADOR ES AZUL
    Public Sub VerGanadorAzul()
        ganadorRojoCount = 1
        If cantidadJugadores = 6 Then
            VerificarPosAzul(10)
        Else
            VerificarPosAzul(15)
        End If
        For cont = 1 To 15
            If cantidadJugadores = 3 Then
                If posiciones(cont) = 65 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 74 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 75 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 84 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 85 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 86 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 95 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 96 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 97 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 98 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 107 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 108 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 109 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 110 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 111 Then
                    ganadorAzul(ganadorRojoCount) = True
                Else
                    ganadorAzul(ganadorRojoCount) = False
                End If
            ElseIf cantidadJugadores = 6 Then
                If posiciones(cont) = 75 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 85 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 86 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 96 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 97 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 98 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 108 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 109 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 110 Then
                    ganadorAzul(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 111 Then
                    ganadorAzul(ganadorRojoCount) = True
                Else
                    ganadorAzul(ganadorRojoCount) = False
                End If
            End If
            ganadorRojoCount += 1
        Next
        If cantidadJugadores = 6 Then
            If Ganado(10, 2) Then
                MsgBox("El jugador " & jugadorres(2) & " ha ganado la partida!!!")
                puntos(2) += 1
                NuevaPartida()
            End If
        Else
            If Ganado(15, 2) Then
                MsgBox("El jugador " & jugadorres(1) & " ha ganado la partida!!!")
                puntos(1) += 1
                NuevaPartida()
            End If
        End If
    End Sub
    'METODO QUE OBTIENE LAS POSCICIONES DE LAS FICHAS ROJAS
    Public Sub VerificarPos(numero As Integer)
        Dim ganador As Integer = 1
        For cont = 1 To numero
            For cont2 = 1 To 121
                If (Canvas.GetLeft(elipArray(cont2)) = Canvas.GetLeft(redPieces(cont))) And (Canvas.GetTop(elipArray(cont2)) = Canvas.GetTop(redPieces(cont))) Then
                    posiciones(ganador) = cont2
                    ganador += 1
                End If
            Next
        Next
    End Sub

    'METODO QUE VERIFICA SI EL GANADOR ES ROJO
    Public Sub VerGanadorRojo()
        ganadorRojoCount = 1
        If cantidadJugadores = 6 Then
            VerificarPos(10)
        Else
            VerificarPos(15)
        End If
        For cont = 1 To 15
            If cantidadJugadores = 6 Then
                If posiciones(cont) = 1 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 2 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 3 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 4 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 5 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 6 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 7 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 8 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 9 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 10 Then
                    ganadorRojo(ganadorRojoCount) = True
                Else
                    ganadorRojo(ganadorRojoCount) = False
                End If
            ElseIf cantidadJugadores = 3 Then
                If posiciones(cont) = 1 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 2 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 3 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 4 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 5 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 6 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 7 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 8 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 9 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 10 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 15 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 16 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 17 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 18 Then
                    ganadorRojo(ganadorRojoCount) = True
                ElseIf posiciones(cont) = 19 Then
                    ganadorRojo(ganadorRojoCount) = True
                Else
                    ganadorRojo(ganadorRojoCount) = False
                End If
            End If
            ganadorRojoCount += 1
        Next
        If cantidadJugadores = 6 Then
            If Ganado(10, 4) Then
                MsgBox("El jugador " & jugadorres(4) & " ha ganado la partida!!!")
                puntos(4) += 1
                NuevaPartida()
            End If
        Else
            If Ganado(15, 4) Then
                MsgBox("El jugador " & jugadorres(2) & " ha ganado la partida!!!")
                puntos(2) += 1
                NuevaPartida()
            End If
        End If
    End Sub
#End Region
End Class